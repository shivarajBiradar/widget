package com.yml.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;

public class MainActivity extends AppWidgetProvider {

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		int currentWidgetId[] = appWidgetIds;

		Intent intent = new Intent(Intent.ACTION_DIAL, null);

		/*
		  Intent facebook=context.getPackageManager().getLaunchIntentForPackage(
		  "com.facebook.katana");
		   Intent gmail=context.getPackageManager().getLaunchIntentForPackage
		 ("com.google.android.gm"); 
		    Intent map=context.getPackageManager().getLaunchIntentForPackage
		 ("com.google.android.apps.maps");
		 */

		Intent gmail = new Intent(Intent.ACTION_VIEW,
				Uri.parse("https://www.gmail.com"));
		Intent map = new Intent(Intent.ACTION_VIEW,
				Uri.parse("https://www.maps.com"));
		Intent facebook = new Intent(Intent.ACTION_VIEW,
				Uri.parse("https://www.facebook.com"));

	

		PendingIntent pending = PendingIntent
				.getActivity(context, 0, intent, 0);
		PendingIntent emailPading = PendingIntent.getActivity(context, 0,
				gmail, 0);

		PendingIntent mapPading = PendingIntent.getActivity(context, 0, map, 0);

		PendingIntent pend = PendingIntent.getActivity(context, 0, facebook, 0);
		RemoteViews views = new RemoteViews(context.getPackageName(),
				R.layout.activity_main);
		views.setOnClickPendingIntent(R.id.imageButton1, pending);
		views.setOnClickPendingIntent(R.id.imageButton3, emailPading);
		views.setOnClickPendingIntent(R.id.imageButton4, mapPading);
		views.setOnClickPendingIntent(R.id.imageButton2, pend);

		appWidgetManager.updateAppWidget(currentWidgetId, views);

		Toast.makeText(context, "widget added", Toast.LENGTH_SHORT).show();
	
	}
}